package com.ilabapps.room.preferences

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import java.sql.Types.NULL

private const val KEY_SAVED_AT = "key_saved_at"
private const val USER_ID = "logged_in_user_id"
class PreferenceProvider(
    context: Context
) {
    private val appContext = context.applicationContext

    private val preference: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(appContext)


    fun savelastSavedAt(savedAt: String) {
        preference.edit().putString(
            KEY_SAVED_AT,
            savedAt
        ).apply()
    }

    fun saveUserID(id:Int){
        preference.edit().putInt(
            USER_ID,
            id
        ).apply()
    }

    fun getUserId():Int?{
        return preference.getInt(USER_ID,NULL)
    }

    fun getLastSavedAt(): String? {
        return preference.getString(KEY_SAVED_AT, null)
    }

    fun clear(): Boolean {
        return preference.edit().clear().commit()
    }

}