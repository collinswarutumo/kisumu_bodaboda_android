package com.ilabapps.room

import android.app.Application
import android.os.Build
import androidx.annotation.RequiresApi
import com.ilabapps.room.data.db.AppDatabase
import com.ilabapps.room.data.network.Api
import com.ilabapps.room.data.network.NetworkConnectionInterceptor
import com.ilabapps.room.data.repositories.HistoryRepository
import com.ilabapps.room.data.repositories.UserRepository
import com.ilabapps.room.data.repositories.VehicleRepository
import com.ilabapps.room.preferences.PreferenceProvider
import com.ilabapps.room.preferences.UserPreferences
import com.ilabapps.room.ui.auth.LoginViewModelFactory
import com.ilabapps.room.ui.history.HistoryViewModelFactory
import com.ilabapps.room.ui.profile.ProfileViewModelFactory
import com.ilabapps.room.ui.vehicle.VehiclesViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class BaseApplication : Application(), KodeinAware {

    @RequiresApi(Build.VERSION_CODES.O)
    override val kodein = Kodein.lazy {
        import(androidXModule(this@BaseApplication))
        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { Api(instance()) }
        bind() from singleton { AppDatabase(instance()) }
        bind() from singleton { PreferenceProvider(instance()) }
        bind() from singleton { UserPreferences(instance()) }
        bind() from singleton { UserRepository(instance(), instance(),instance()) }
        bind() from singleton { HistoryRepository(instance(), instance(),instance(),instance()) }
        bind() from singleton { VehicleRepository(instance()) }
        bind() from provider { LoginViewModelFactory(instance(),instance()) }
        bind() from provider { VehiclesViewModelFactory(instance()) }
        bind() from provider { ProfileViewModelFactory(instance()) }
        bind() from provider { HistoryViewModelFactory(instance()) }

    }
}