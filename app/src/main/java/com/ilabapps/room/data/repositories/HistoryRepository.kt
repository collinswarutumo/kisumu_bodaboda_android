package com.ilabapps.room.data.repositories

import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ilabapps.room.data.db.AppDatabase
import com.ilabapps.room.data.db.entity.HistoryEntity
import com.ilabapps.room.data.network.Api
import com.ilabapps.room.data.network.SafeApiRequest
import com.ilabapps.room.preferences.PreferenceProvider
import com.ilabapps.room.util.Coroutines
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

private val MINIMUM_INTERVAL = 60000

class HistoryRepository(
    private val api: Api,
    private val db: AppDatabase,
    private val prefs: PreferenceProvider,
    private  val sharedPreferences: SharedPreferences
) : SafeApiRequest() {
    private val histories = MutableLiveData<List<HistoryEntity>>()

    init {
        histories.observeForever {
            saveUserHistories(it)
        }
    }

    private suspend fun fetchHistories() {
        val lastSavedAt = prefs.getLastSavedAt()
        if (lastSavedAt == null || isFetchNeeded(lastSavedAt)) {
            val response = apiRequest { api.getUserHistory() }
            histories.postValue(response.usertransactions)
        }
    }

    suspend fun getUserHistory(): LiveData<List<HistoryEntity>> {
        return withContext(Dispatchers.IO) {
            fetchHistories()
            db.historyDao().getUserHistory()
        }
    }

    private fun isFetchNeeded(savedAt: String?): Boolean {
        return savedAt + MINIMUM_INTERVAL < System.currentTimeMillis().toString()
    }

    private fun saveUserHistories(histories: List<HistoryEntity>) {
        Coroutines.io {
            prefs.savelastSavedAt(System.currentTimeMillis().toString())
            db.historyDao().saveAllHistories(histories)
        }
    }
}
