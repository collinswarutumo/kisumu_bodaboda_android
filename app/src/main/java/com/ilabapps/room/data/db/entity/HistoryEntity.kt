package com.ilabapps.room.data.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class HistoryEntity (
    @PrimaryKey(autoGenerate = false)
    val transaction_id:Int,
    val phone_num:String,
    val amount:String,
    val account_reference:String,
    val server_date:String,
)