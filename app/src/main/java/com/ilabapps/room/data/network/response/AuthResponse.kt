package com.ilabapps.room.data.network.response

import com.ilabapps.room.data.db.entity.UserEntity

data class AuthResponse (val result:String, val user:UserEntity?)