package com.ilabapps.room.data.network.response
import com.ilabapps.room.data.db.entity.VehicleEntity


data class VehicleResponse (val result:String , val vehicles: VehicleEntity? )