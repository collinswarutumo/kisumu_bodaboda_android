package com.ilabapps.room.data.models


import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("emailaddress")
    val emailaddress: String,
    @SerializedName("idnumber")
    val idnumber: String,
    @SerializedName("mobilenumber")
    val mobilenumber: String,
    @SerializedName("names")
    val names: String,
    @SerializedName("physicaladdress")
    val physicaladdress: String,
    @SerializedName("subcounty_name")
    val subcountyName: String,
    @SerializedName("userid")
    val userid: String,
    @SerializedName("ward_name")
    val wardName: String
)