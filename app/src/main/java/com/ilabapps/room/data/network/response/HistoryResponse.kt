package com.ilabapps.room.data.network.response

import com.ilabapps.room.data.db.entity.HistoryEntity

data class HistoryResponse(val usertransactions:List<HistoryEntity>)