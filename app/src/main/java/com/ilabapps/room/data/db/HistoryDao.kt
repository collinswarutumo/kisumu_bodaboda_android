package com.ilabapps.room.data.db
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ilabapps.room.data.db.entity.HistoryEntity

@Dao
interface HistoryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAllHistories(histories: List<HistoryEntity>)

    @Query("SELECT * FROM HistoryEntity")
    fun getUserHistory():LiveData<List<HistoryEntity>>
}