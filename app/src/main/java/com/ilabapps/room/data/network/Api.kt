package com.ilabapps.room.data.network
import com.ilabapps.room.data.models.Vehicle
import com.ilabapps.room.data.network.response.AuthResponse
import com.ilabapps.room.data.network.response.HistoryResponse
import com.ilabapps.room.data.network.response.VehicleResponse
import com.ilabapps.room.preferences.PreferenceProvider
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*


interface Api {

    @GET("uservehicles")

    suspend fun getUserVehicles() : Response<List<Vehicle>>
    @GET("sticker")
    fun getSticker(

    )

    @GET(value = "usertransactions")
    suspend fun getUserHistory(): Response<HistoryResponse>


    @GET("userdetails")
    fun getProfile(

    )
    @POST("registerVehicles/37")
    fun postVehicle(
        @Field("model_id") model_id: String,
        @Field("make_id") make_id: String,
        @Field("category_id") category_id: String,
        @Field("capacity_id") capacity_id: String,
        @Field("zone_id") zone_id: String,
        @Field("registration_no") registration_no: String,
        @Field("subcounty_id") subcounty_id: String,
        @Field("ward_id") ward_id: String,
    ): Response<VehicleResponse>
    @PUT("userdetail")
    fun putUserDetail(

    )
    @FormUrlEncoded
    @POST("auth")
    suspend fun userLogin(
        @Field("idnumber") idnumber: String,
        @Field("password") password: String
    ) : Response<AuthResponse>

    @FormUrlEncoded
    @POST("registerUser")
    suspend fun userSignup(
        @Field("firstName") firstName: String,
        @Field("lastName") lastName: String,
        @Field("mobileNumber") mobileNumber: String,
        @Field("emailAddress") emailAddress: String,
        @Field("idDocType") idDocType: String,
        @Field("idNumber") idNumber: String,
        @Field("physicalAddress") physicalAddress: String,
        @Field("subcounty_id") subcounty_id: String,
        @Field("ward_id") ward_id: String,
        @Field("password") password: String
    ) : Response<AuthResponse>

    companion object{
        operator fun invoke(networkConnectionInterceptor: NetworkConnectionInterceptor,  token: String? = null) : Api {

            val okkHttpclient = OkHttpClient.Builder()
                    .addInterceptor { chain ->
                        chain.proceed(chain.request().newBuilder().also {
                            it.addHeader("Authorization", "$token")
                        }.build())
                    }
                    .addNetworkInterceptor(networkConnectionInterceptor)
                    .build()

                return Retrofit.Builder()
                        .baseUrl("http://cpmini.strathmore.edu/cpmini_ussd_kisumu_test/api/")
                        .client(okkHttpclient)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()
                        .create(Api::class.java)
            }

    }
}


