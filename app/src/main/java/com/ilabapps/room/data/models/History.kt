package com.ilabapps.room.data.models


import com.google.gson.annotations.SerializedName

data class History(
    @SerializedName("account_reference")
    val accountReference: String,
    @SerializedName("amount")
    val amount: String,
    @SerializedName("phone_num")
    val phoneNum: String,
    @SerializedName("server_date")
    val serverDate: String
)