package com.ilabapps.room.data.db.entity
import androidx.room.Entity
import androidx.room.PrimaryKey

const val CURRENT_USER_ID = 0
@Entity
data class UserEntity(
    var userid : Int? = null,
    val names: String?= null,
    val emailaddress: String?= null,
    val mobilenumber: String?= null,
    val physicaladdress: String?= null,
    val ward_name: String?= null,
    val subcounty_name: String?= null,
    val idnumber: String?= null,
    val token:String? = null

){
    @PrimaryKey(autoGenerate = false)
    var uuserid: Int = CURRENT_USER_ID
}