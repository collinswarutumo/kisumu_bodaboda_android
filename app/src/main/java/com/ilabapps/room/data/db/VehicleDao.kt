package com.ilabapps.room.data.db
import androidx.room.*
import com.ilabapps.room.data.db.entity.VehicleEntity


@Dao
interface VehicleDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addVehicle(vehicle: VehicleEntity)
    @Query("SELECT * FROM VehicleEntity ORDER BY pb_vehicle_id DESC")
    suspend fun getAllVehicles() : List<VehicleEntity>

}