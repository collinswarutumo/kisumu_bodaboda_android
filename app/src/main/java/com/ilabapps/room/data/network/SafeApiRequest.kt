package com.ilabapps.room.data.network

import android.util.Log
import com.ilabapps.room.util.ApiException
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response


abstract class SafeApiRequest {
    suspend fun <T : Any> apiRequest(call: suspend () -> Response<T>): T {
        val response = call.invoke()
        if (response.isSuccessful) {
            return response.body()!!
        } else {
            val error = response.errorBody()?.string()

            val result = StringBuilder()
            error?.let{
                try{
                    result.append(JSONObject(it).getString("result"))
                }catch(e: JSONException){ }
                result.append("\n")
            }
            result.append("Error Code: ${response.code()}")
            throw ApiException(result.toString())
            Log.d("Main",result.toString())
        }
    }
}