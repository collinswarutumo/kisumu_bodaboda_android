package com.ilabapps.room.data.network

import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import androidx.annotation.RequiresApi
import com.ilabapps.room.util.NoInternetException
import okhttp3.Interceptor
import okhttp3.Response


class NetworkConnectionInterceptor(context: Context) : Interceptor{
    private val applicationContext = context.applicationContext
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun intercept(chain: Interceptor.Chain):  Response{
        if (!isInternetAvailable())
            throw NoInternetException("You May Not Have an Active Data Connection")
        return chain.proceed(chain.request())
    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun isInternetAvailable(): Boolean {

        val connectivityManager =
            applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivityManager.activeNetworkInfo.also {
            return it != null && it.isConnected
        }
    }
}