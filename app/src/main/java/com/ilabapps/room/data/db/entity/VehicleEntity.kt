package com.ilabapps.room.data.db.entity


import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity

 class VehicleEntity(

    val model_id: String,
    val make_id: String,
    val category_id: String,
    val capacity_id: String,
    val zone_id: String,
    val registration_no: String,
    val subcounty_id: String,
    val ward_id: String,


):Serializable{
    @PrimaryKey(autoGenerate = true)
    var pb_vehicle_id : Int = 0
}

