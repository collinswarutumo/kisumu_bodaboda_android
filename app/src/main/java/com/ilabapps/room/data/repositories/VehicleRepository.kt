package com.ilabapps.room.data.repositories
import com.ilabapps.room.data.network.Api
import com.ilabapps.room.data.network.SafeApiRequest
import com.ilabapps.room.data.network.response.VehicleResponse

class VehicleRepository(
    private val api:Api
) : SafeApiRequest() {
    suspend fun getUserVehicles() = apiRequest { api.getUserVehicles() }
     suspend fun postVehicle(
        model_id:String,
        make_id:String,
        category_id:String,
        capacity_id:String,
        zone_id:String,
        registration_no:String,
        subcounty_id:String,
        ward_id: String
    ) : VehicleResponse {
        return apiRequest{ api.postVehicle(  model_id, make_id, category_id, capacity_id, zone_id, registration_no, subcounty_id, ward_id)}
    }
}