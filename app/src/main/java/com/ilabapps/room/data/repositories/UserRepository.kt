package com.ilabapps.room.data.repositories
import com.ilabapps.room.data.db.AppDatabase
import com.ilabapps.room.data.db.entity.UserEntity
import com.ilabapps.room.data.network.Api
import com.ilabapps.room.data.network.SafeApiRequest
import com.ilabapps.room.data.network.response.AuthResponse
import com.ilabapps.room.preferences.PreferenceProvider
import com.ilabapps.room.preferences.UserPreferences


/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */

class UserRepository(
    private val api: Api,
    private val db: AppDatabase,
    private val prefs: UserPreferences
): SafeApiRequest() {

    suspend fun saveUser(user: UserEntity) = db.userDao().upsert(user)

    suspend fun userLogin(idnumber: String, password: String): AuthResponse {
        return apiRequest { api.userLogin(idnumber, password) }

    }
    fun getUser() = db.userDao().getuser()

    suspend fun userSignup(
        firstName: String,
        lastName: String,
        mobileNumber: String,
        emailAddress:String,
        idDocType: String,
        idNumber:String,
        physicalAddress:String,
        subcounty_id: String,
        ward_id:String,
        password: String
    ) : AuthResponse {
        return apiRequest{ api.userSignup(firstName , lastName , mobileNumber, emailAddress, idDocType, idNumber, physicalAddress, subcounty_id, ward_id, password)}
    }

    suspend fun saveUserToken(token: String){
        prefs.saveToken(token)
    }

}