package com.ilabapps.room.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ilabapps.room.data.db.entity.CURRENT_USER_ID
import com.ilabapps.room.data.db.entity.UserEntity

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsert(UserEntity: UserEntity ) : Long

    @Query("SELECT * FROM  UserEntity WHERE uuserid = $CURRENT_USER_ID")
    fun getuser() : LiveData<UserEntity>


}