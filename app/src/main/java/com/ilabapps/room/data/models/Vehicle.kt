package com.ilabapps.room.data.models
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class Vehicle(


    @SerializedName("make")
    val make: String,
    @SerializedName("mobilenumber")
    val mobilenumber: String,
    @SerializedName("model")
    val model: String,
    @SerializedName("registration_no")
    val registrationNo: String,
    @SerializedName("subcounty_name")
    val subcountyName: String,
    @SerializedName("vehicle_category")
    val vehicleCategory: String,
    @SerializedName("ward_name")
    val wardName: String
)