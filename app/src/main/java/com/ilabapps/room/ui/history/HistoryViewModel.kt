package com.ilabapps.room.ui.history

import androidx.lifecycle.ViewModel
import com.ilabapps.room.data.repositories.HistoryRepository
import com.ilabapps.room.util.lazyDeferred

class HistoryViewModel(
    repository: HistoryRepository
) : ViewModel() {
val histories by lazyDeferred {
    repository.getUserHistory()
}

}