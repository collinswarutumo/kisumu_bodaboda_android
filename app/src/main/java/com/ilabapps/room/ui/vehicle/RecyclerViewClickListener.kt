package com.ilabapps.room.ui.vehicle

import android.view.View
import com.ilabapps.room.data.models.Vehicle

interface RecyclerViewClickListener {
    fun onRecyclerViewClickListener(view: View, Vehicle: Vehicle)
}