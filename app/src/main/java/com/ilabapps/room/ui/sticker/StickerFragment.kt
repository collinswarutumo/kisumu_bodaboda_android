package com.ilabapps.room.ui.sticker

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ilabapps.room.R
import com.ilabapps.room.ui.profile.StickerViewModel

class StickerFragment : Fragment() {

    private lateinit var stickerViewModel: StickerViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        stickerViewModel =
                ViewModelProviders.of(this).get(StickerViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_sticker, container, false)
        val textView: TextView = root.findViewById(R.id.text_sticker)
        stickerViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        return root
    }
}