package com.ilabapps.room.ui.profile
import androidx.lifecycle.ViewModel
import com.ilabapps.room.data.repositories.UserRepository

class ProfileViewModel (
    repository: UserRepository
) : ViewModel() {
 
    val user = repository.getUser()
}