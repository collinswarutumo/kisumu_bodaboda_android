package com.ilabapps.room.ui.auth

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.ilabapps.room.MainActivity
import com.ilabapps.room.R
import com.ilabapps.room.data.models.User
import com.ilabapps.room.databinding.ActivityLoginBinding
import com.ilabapps.room.preferences.UserPreferences
import com.ilabapps.room.util.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class LoginActivity : AppCompatActivity(), KodeinAware {
    override val kodein by kodein()
    private val factory: LoginViewModelFactory by instance()
    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewModel: LoginViewModel
    protected lateinit var userPreferences : UserPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userPreferences = UserPreferences(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewModel = ViewModelProvider(this, factory).get(LoginViewModel::class.java)
        binding.loginButton.enable(false)
        viewModel.getLoggedInUser().observe(this, Observer { UserEntity ->
            binding.progressBar.visible(false)
            if (UserEntity != null) {
                Intent(this, MainActivity::class.java).also {
                    it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(it)
                }
            }
        })
        binding.loginPassword.addTextChangedListener {
            val email = binding.loginEmail.text.toString().trim()
            binding.loginButton.enable(email.isNotEmpty() && it.toString().isNotEmpty())
        }
        binding.loginButton.setOnClickListener {
            loginUser()
        }
        binding.loginRegister.setOnClickListener {
            startActivity(Intent(this, SignupActivity::class.java))
        }
    }

    private fun loginUser() {
        binding.progressBar.visible(true)

        val idnumber = binding.loginEmail.text.toString().trim()
        val password = binding.loginPassword.text.toString().trim()

        if (idnumber.isNullOrEmpty() || password.isNullOrEmpty()) {
            binding.progressBar.visible(false)
            Snackbar.make(
                    binding.rootLayout,
                    "ID NUMBER AND PASSWORD REQUIRED",
                    Snackbar.LENGTH_SHORT
            ).show()
            return
        }

        lifecycleScope.launch {

            try {
                val authResponse = viewModel.userLogin(idnumber, password)

                if (authResponse.user != null) {
                    viewModel.saveLoggedInUser(authResponse.user)
                    viewModel.saveUserToken(authResponse.user.token!!)
                    binding.progressBar.visible(false)

                } else {

                    val text = authResponse.result
                    val duration = Toast.LENGTH_SHORT

                    val toast = Toast.makeText(applicationContext, text, duration)
                    toast.show()

                    Snackbar.make(binding.rootLayout, authResponse.result, Snackbar.LENGTH_SHORT)
                            .show()
                    binding.rootLayout.snackbar(authResponse.result)
                    binding.progressBar.visible(false)
                }
            } catch (e: ApiException) {
                e.printStackTrace()
                Snackbar.make(binding.rootLayout, e.message.toString(), Snackbar.LENGTH_SHORT).show()
                binding.progressBar.visible(false)

            } catch (e: NoInternetException) {
                e.printStackTrace()
                Snackbar.make(binding.rootLayout, e.message.toString(), Snackbar.LENGTH_SHORT)
                        .show()
                binding.progressBar.visible(false)
            }
        }
    }


}

