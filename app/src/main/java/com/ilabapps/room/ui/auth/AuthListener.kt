package com.ilabapps.room.ui.auth

import androidx.lifecycle.LiveData
import com.ilabapps.room.data.db.entity.UserEntity

interface AuthListener {
    fun onStarted()
    fun onSuccess(userEntity: UserEntity)
    fun onFailure(message:String)
}