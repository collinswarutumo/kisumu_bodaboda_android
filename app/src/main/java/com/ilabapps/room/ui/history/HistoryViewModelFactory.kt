package com.ilabapps.room.ui.history

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ilabapps.room.data.repositories.HistoryRepository

@Suppress("UNCHECKED_CAST")
class HistoryViewModelFactory(private val repository: HistoryRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HistoryViewModel(repository) as T
    }
}