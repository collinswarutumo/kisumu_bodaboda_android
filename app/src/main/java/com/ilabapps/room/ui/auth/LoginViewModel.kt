package com.ilabapps.room.ui.auth
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ilabapps.room.data.db.entity.UserEntity
import com.ilabapps.room.data.network.Resource
import com.ilabapps.room.data.network.response.AuthResponse
import com.ilabapps.room.data.repositories.UserRepository
import com.ilabapps.room.preferences.PreferenceProvider
import com.ilabapps.room.preferences.UserPreferences
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class LoginViewModel(
    private val repository: UserRepository,
    private val prefs: PreferenceProvider

) : ViewModel() {
    //Auth

    fun getLoggedInUser() = repository.getUser()

    suspend fun userLogin(
        idnumber: String,
        password: String
    ) =  repository.userLogin(idnumber, password)

    suspend fun saveLoggedInUser(userEntity: UserEntity) = repository.saveUser(userEntity)
    suspend fun userSignup(
        firstName: String,
        lastName: String,
        mobileNumber: String,
        emailAddress:String,
        idDocType: String,
        idNumber:String,
        physicalAddress:String,
        subcounty_id: String,
        ward_id:String,
        password: String
    ) = withContext(Dispatchers.IO) { repository.userSignup(firstName,lastName,mobileNumber, emailAddress, idDocType, idNumber, physicalAddress, subcounty_id , ward_id, password) }

    fun saveUserToken(token:String) = viewModelScope.launch {
        repository.saveUserToken(token)
    }
}













