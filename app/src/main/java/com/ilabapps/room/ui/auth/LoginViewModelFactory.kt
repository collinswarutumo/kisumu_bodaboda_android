package com.ilabapps.room.ui.auth

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ilabapps.room.data.repositories.UserRepository
import com.ilabapps.room.preferences.PreferenceProvider

@Suppress("UNCHECKED_CAST")
class LoginViewModelFactory(private val repository: UserRepository, private val prefs: PreferenceProvider): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LoginViewModel(repository,prefs) as T
    }
}