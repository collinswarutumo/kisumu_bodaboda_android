package com.ilabapps.room.ui.vehicle

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import com.google.android.material.snackbar.Snackbar
import com.ilabapps.room.R
import com.ilabapps.room.data.db.AppDatabase
import com.ilabapps.room.data.db.entity.VehicleEntity
import com.ilabapps.room.databinding.ActivitySignupBinding
import com.ilabapps.room.ui.toast
import com.ilabapps.room.util.*
import kotlinx.android.synthetic.main.add_vehicle_fragment.*
import kotlinx.coroutines.launch

class AddVehicleFragment : Fragment() {

    private var vehicle: VehicleEntity? = null

    companion object {
        fun newInstance() = AddVehicleFragment()
    }

    private lateinit var viewModel: AddVehicleViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.add_vehicle_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        arguments?.let {


        }
        button_save.setOnClickListener { view ->
            Log.d("Main", "Imefika")
            val model_id = tv_model.text.toString().trim()
            val make_id = tv_make.text.toString().trim()
            val category_id = tv_category.text.toString().trim()
            val capacity_id = tv_capacity.text.toString().trim()
            val zone_id = tv_zone.text.toString().trim()
            val registration_no = tv_regno.text.toString().trim()
            val subcounty_id = tv_subcounty.text.toString().trim()
            val ward_id = tv_ward.text.toString().trim()

            lifecycleScope.launch {


             viewModel.postVehicle(
                    model_id,
                    make_id,
                    category_id,
                    capacity_id,
                    zone_id,
                    registration_no,
                    subcounty_id,
                    ward_id
                )



                context?.let {
//                    AppDatabase(it).vehicleDao().addVehicle(vehicle_database)
                    it.toast("Vehicle Saved")

                    val action = AddVehicleFragmentDirections.actionSaveVehicle()
                    Navigation.findNavController(view).navigate(action)
                }
            }


            }
        }

    }

