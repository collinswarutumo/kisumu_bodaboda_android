package com.ilabapps.room.ui.vehicle

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.ilabapps.room.R
import com.ilabapps.room.data.models.Vehicle
import com.ilabapps.room.data.network.Api
import com.ilabapps.room.data.network.NetworkConnectionInterceptor
import com.ilabapps.room.data.repositories.VehicleRepository
import com.ilabapps.room.preferences.UserPreferences
import com.ilabapps.room.util.hide
import com.ilabapps.room.util.show
import kotlinx.android.synthetic.main.fragment_history.*
import kotlinx.android.synthetic.main.fragment_vehicle.*
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking


class VehicleFragment : Fragment(), RecyclerViewClickListener {

    private lateinit var vehicleViewModel: VehicleViewModel
    private lateinit var factory: VehiclesViewModelFactory
    protected lateinit var userPreferences: UserPreferences
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        userPreferences = UserPreferences(requireContext())
        lifecycleScope.launch { userPreferences.token.first() }
        return inflater.inflate(R.layout.fragment_vehicle, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val networkConnectionInterceptor = NetworkConnectionInterceptor(activity?.applicationContext!!)
         val token = runBlocking {userPreferences.token.first()  }

        val api = Api(networkConnectionInterceptor,token)
        val repository = VehicleRepository(api)

        factory = VehiclesViewModelFactory(repository)
        vehicleViewModel = ViewModelProviders.of(this, factory).get(VehicleViewModel::class.java)

        vehicleViewModel.getVehicles()

        vehicleViewModel.vehicles.observe(viewLifecycleOwner, Observer { vehicles ->

            recycler_view_vehicles.also {

                it.layoutManager = LinearLayoutManager(requireContext())
                it.setHasFixedSize(true)
                it.adapter = VehiclesAdapter(vehicles, this)

            }

        })

        button_add.setOnClickListener{ it->
            val action = VehicleFragmentDirections.actionAddvehicle()
            Navigation.findNavController(it).navigate(action)

        }
    }

    override fun onRecyclerViewClickListener(view: View, Vehicle: Vehicle) {
        when (view.id) {
            R.id.button_book -> {
                Toast.makeText(
                    requireContext(),
                    "Vehicle Disable Button Clicked",
                    Toast.LENGTH_LONG
                ).show()

            }
        }
    }

}