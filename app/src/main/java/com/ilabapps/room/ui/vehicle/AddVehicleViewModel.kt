package com.ilabapps.room.ui.vehicle

import androidx.lifecycle.ViewModel
import com.ilabapps.room.data.repositories.VehicleRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AddVehicleViewModel(private val repository: VehicleRepository) : ViewModel() {
     suspend fun postVehicle(
        model_id:String,
        make_id:String,
        category_id:String,
        capacity_id:String,
        zone_id:String,
        registration_no:String,
        subcounty_id:String,
        ward_id: String
    ) = repository.postVehicle(model_id, make_id, category_id, capacity_id, zone_id, registration_no, subcounty_id, ward_id)
}