package com.ilabapps.room.ui.vehicle

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ilabapps.room.data.repositories.VehicleRepository

@Suppress("UNCHECKED_CAST")
class VehiclesViewModelFactory(
    private val repository: VehicleRepository
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return VehicleViewModel(repository) as T
    }
}