package com.ilabapps.room.ui.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager

import com.ilabapps.room.R
import com.ilabapps.room.data.db.entity.HistoryEntity
import com.ilabapps.room.util.Coroutines
import com.ilabapps.room.util.hide
import com.ilabapps.room.util.show
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.fragment_history.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.util.Collections.addAll

class HistoryFragment : Fragment(),KodeinAware {
   override val kodein by kodein()
    private val factory: HistoryViewModelFactory by instance()
    private lateinit var viewModel: HistoryViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_history, container, false)
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, factory).get(HistoryViewModel::class.java)
        bindUI()
    }
    private fun bindUI() = Coroutines.main {
        progress_bar.show()
      viewModel.histories.await().observe(viewLifecycleOwner, Observer {
          progress_bar.hide()
          InitRecylerView(it.toHistoryItem())

        })
    }

    private fun InitRecylerView(historyItem: List<HistoryItem>) {
        val mAdapter = GroupAdapter<GroupieViewHolder>().apply {
            addAll(historyItem)
        }


        recycler_view_history.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = mAdapter
        }

    }

    private fun List<HistoryEntity>.toHistoryItem() : List<HistoryItem>{
        return this.map {
            HistoryItem(it)
        }
    }

}