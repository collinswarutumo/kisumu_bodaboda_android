package com.ilabapps.room.ui.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.ilabapps.room.MainActivity
import com.ilabapps.room.R
import com.ilabapps.room.data.db.entity.UserEntity
import com.ilabapps.room.databinding.ActivitySignupBinding
import com.ilabapps.room.util.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class SignupActivity : AppCompatActivity(), KodeinAware {
    override val kodein by kodein()
    private val factory: LoginViewModelFactory by instance()
    private lateinit var binding: ActivitySignupBinding
    private lateinit var viewModel: LoginViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup)
        viewModel = ViewModelProvider(this, factory).get(LoginViewModel::class.java)


        viewModel.getLoggedInUser().observe(this, Observer {UserEntity ->

            if (UserEntity != null) {
                Intent(this, MainActivity::class.java).also {
                    it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(it)
                }
            }
        })
        binding.tvLogin.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }

        binding.buttonRegister.setOnClickListener{
            userSignup()
        }
    }

    private fun userSignup() {


        val firstName = binding.tvFirstname.text.toString().trim()
        val lastName = binding.tvLastname.text.toString().trim()
        val mobileNumber = binding.tvMobilenumber.text.toString().trim()
        val emailAddress = binding.tvEmail.text.toString().trim()
        val idDocType = binding.tvIddoctype.text.toString().trim()
        val idNumber = binding.tvIdnumber.text.toString().trim()
        val physicalAddress = binding.tvPhysicaladdress.text.toString().trim()
        val subcounty_id = binding.tvSubcounty.text.toString().trim()
        val ward_id = binding.tvWard.text.toString().trim()
        val registerpassword = binding.tvPassword.text.toString().trim()
        val password1 = binding.tvPasswordconfirm.text.toString().trim()

         binding.progressBar.show()
        if(firstName.isNullOrEmpty() || lastName.isNullOrEmpty() || mobileNumber.isNullOrEmpty() || emailAddress.isNullOrEmpty() || idDocType.isNullOrEmpty() || idNumber.isNullOrEmpty() || physicalAddress.isNullOrEmpty() || subcounty_id.isNullOrEmpty() || ward_id.isNullOrEmpty() || registerpassword.isNullOrEmpty()){
            binding.progressBar.hide()
            Snackbar.make(binding.rootLayout,"Missing Required Parameters" , Snackbar.LENGTH_SHORT)
                .show()
            return
        }

        if(registerpassword!=password1){
            binding.progressBar.hide()
            Snackbar.make(binding.rootLayout,"Password Does Not Match" , Snackbar.LENGTH_SHORT)
                .show()
            return
        }

        lifecycleScope.launch {
            try {
                val authResponse = viewModel.userSignup(firstName,lastName,mobileNumber, emailAddress, idDocType, idNumber, physicalAddress, subcounty_id , ward_id, registerpassword)
                Log.d("Main",authResponse.toString())
                if (authResponse.user != null) {
                       binding.progressBar.hide()
                    viewModel.saveLoggedInUser(authResponse.user)
                    Log.d("Main",authResponse.user.toString())
                } else {
                     binding.progressBar.hide()
                  Snackbar.make(binding.rootLayout,authResponse.result, Snackbar.LENGTH_SHORT)
                       .show()
                    binding.root.snackbar(authResponse.result!!)
                    Log.d("Main",authResponse.result)
                }
                binding.progressBar.hide()
            } catch (e: ApiException) {
               Snackbar.make(binding.rootLayout,e.printStackTrace().toString() , Snackbar.LENGTH_SHORT)
                   .show()
                e.printStackTrace()
            } catch (e: NoInternetException) {
                e.printStackTrace()
                Snackbar.make(binding.rootLayout,e.printStackTrace().toString(), Snackbar.LENGTH_SHORT)
                    .show()
            }
        }
    }

}