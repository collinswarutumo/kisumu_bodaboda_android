package com.ilabapps.room.ui.vehicle

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.ilabapps.room.R
import com.ilabapps.room.data.models.Vehicle
import com.ilabapps.room.databinding.RecyclerviewVehicleBinding
import com.ilabapps.room.ui.vehicle.RecyclerViewClickListener
class VehiclesAdapter(
    private val vehicles: List<Vehicle>,
    private val listener: RecyclerViewClickListener
) : RecyclerView.Adapter<VehiclesAdapter.VehiclesViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) = VehiclesViewHolder(DataBindingUtil.inflate(
        LayoutInflater.from(parent.context),
        R.layout.recyclerview_vehicle,
        parent,
        false
    ))


    override fun onBindViewHolder(holder: VehiclesViewHolder, position: Int) {
        holder.recyclerviewVehicleBinding.vehicle = vehicles[position]
        holder.recyclerviewVehicleBinding.buttonBook.setOnClickListener {
            listener.onRecyclerViewClickListener(holder.recyclerviewVehicleBinding.buttonBook,vehicles[position])
        }
    }

    override fun getItemCount() = vehicles.size

    inner class VehiclesViewHolder(
        val recyclerviewVehicleBinding: RecyclerviewVehicleBinding
    ) : RecyclerView.ViewHolder(recyclerviewVehicleBinding.root)


}