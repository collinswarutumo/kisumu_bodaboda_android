package com.ilabapps.room.ui.history
import com.ilabapps.room.R
import com.ilabapps.room.data.db.entity.HistoryEntity
import com.ilabapps.room.databinding.ItemHistoryBinding
import com.xwray.groupie.databinding.BindableItem


class HistoryItem(private  val history: HistoryEntity) : BindableItem<ItemHistoryBinding>(){
    override fun bind(viewBinding: ItemHistoryBinding, position: Int) {
       viewBinding.setHistory(history)
    }

    override fun getLayout()= R.layout.item_history

}
