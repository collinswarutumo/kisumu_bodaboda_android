package com.ilabapps.room.ui.vehicle


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ilabapps.room.data.models.Vehicle
import com.ilabapps.room.data.network.Resource
import com.ilabapps.room.data.repositories.VehicleRepository
import com.ilabapps.room.util.Coroutines
import com.ilabapps.room.util.show
import kotlinx.android.synthetic.main.fragment_history.*
import kotlinx.coroutines.Job


class VehicleViewModel(
    private val repository: VehicleRepository
) : ViewModel() {
    private lateinit var job: Job
    private val _vehicles = MutableLiveData<List<Vehicle>>()
    val vehicles: LiveData<List<Vehicle>>
        get() = _vehicles

    fun getVehicles() {

        job = Coroutines.ioThenMain(

            { repository.getUserVehicles() },
            { _vehicles.value = it }
        )

    }
    override fun onCleared() {
        super.onCleared()
        if(::job.isInitialized) job.cancel()
    }
}