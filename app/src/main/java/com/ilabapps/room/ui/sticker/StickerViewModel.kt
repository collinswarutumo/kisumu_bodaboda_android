package com.ilabapps.room.ui.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class StickerViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is sticker Fragment"
    }
    val text: LiveData<String> = _text
}